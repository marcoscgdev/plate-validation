test("Valid License Plate", function (assert) {
    assert.equal(isValidPlate("1234DFJ"), true, "VALID LICENSE PLATE");
    assert.equal(isValidPlate("4292HPK"), true, "VALID LICENSE PLATE");
    assert.equal(isValidPlate("9834KLH"), true, "VALID LICENSE PLATE");
    assert.equal(isValidPlate("2964JFK"), true, "VALID LICENSE PLATE");
});

test("Invalid Arguments", function (assert) {
    assert.equal(isValidPlate("1234"), false , "ONLY NUMBERS");
    assert.equal(isValidPlate("FHJ"), false , "ONLY LETTERS");
    assert.equal(isValidPlate("1234ABC"), false , "INVALID LETTER");
    assert.equal(isValidPlate("1.34DKH"), false, "WRONG CHARACTER");
});
