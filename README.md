# Plate Validation

Plate Verification software using javascript. It is part of the task "SCM for the plate verification software" of the subject Software Quality (San Jorge university).

[Click here](https://marcoscgdev.gitlab.io/plate-validation/) to see it in action.